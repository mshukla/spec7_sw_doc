.. SPEC7 documentation master file, created by
   sphinx-quickstart on Mon Jun 28 00:25:37 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SPEC7 documentation
=================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   introduction
   hdl
   fsbl
   uboot
   build
   kernel
   

