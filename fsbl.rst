.. _fsbl:

FSBL
=========
FSBL (First stage bootloader) built using Vitis v2019.2 after successfully 
generating the bitstream for above design and exporting as hardware xsa
file in the platform. This provides a golden design along with FSBL, 
which can be booted via writing to QSPI Flash Memory

Build
------

Using Vitis GUI
++++++++++++++++++

Steps to build:

1. Export the design as xsa file for later developments in Vitis
2. Add platform project name and location 
3. Modify BSP Settings and enable xilffs
4. Add application project and select ZYNQ_FSBL
5. Go to Next
6. Check the sources initialised on left side 
7. Run Build

More details refer `this <https://ohwr.org/project/spec7/wikis/uploads/ca8f150af87f5119faa94b84539f0ea0/SPEC7.pdf>`_


Command line
++++++++++++++++++

Require: Vitis 2019.2

1. SetUp Environment 

.. code-block:: 

      $ source /tools/Xilinx/Vitis/2019.2/settings64.sh

2. Enable Xilinx Software Command Line Tool

.. code-block::

      $ xsct%

3. Setup workspace

.. code-block::

      $ setws </path/to/a/directory>

4. Export xsa file and setup platform project

.. code-block::

      $ platform create -name <platform/name eg:spec7_custom>  -hw </<path to xsa>/spec7_custom.xsa> -no-boot-bsp

5. Check the active platform:
   
.. code-block::
  
      $ platform active

6. Create domain

.. code-block::
     
      $ domain create -name "fsbl_domain" -os standalone -proc ps7_cortexa9_0


7. Check the active domain with:

.. code-block::
   
      $ domain active

8. Add library before building FSBL:
   
.. code-block::

      $ bsp setlib xilffs

9. Check stdin and stdout configuration in the BSP:
   
.. code-block::
      
      $ bsp config stdin ps7_uart_1
      $ bsp config stdout ps7_uart_1


10. Build the platform:
    
.. code-block::

       $ platform generate

11. Add system project

First, we create the application for the FSBL targeting the already existing platform and the specific domain and specifying the template Zynq FSBL. In addition, we will select the spec7_custom_system as the name of the system that will be created to host the application:

    .. code-block::

        $ app create -name zynq_fsbl -template {Zynq  FSBL} -platform spec7_custom -domain fsbl_domain -sysproj spec7_custom_system


12. Config and Build FSBL:

    .. code-block::

       $ app config -name zynq_fsbl build-config release

       $ app config -name zynq_fsbl build-config

       $ app build -name zynq_fsbl

This will generate ``zynq_fsbl.elf`` file in workspace directory set using step 2  as ``<path/to/directory_ws/zynq_fsbl/Release/zynq_fsbl.elf>``.  


Tcl script
+++++++++++++++++++++


All the above steps can be automated using Tcl script and built with make.

Source: In `spec7 ohwr repo <https://ohwr.org/project/spec7/tree/spec7_golden/sw/fsbl>`_   ``sw/fsbl/``, run 

.. code-block::
        
        $ make

This will generate zynq_fsbl.elf in ``/output`` directory.


