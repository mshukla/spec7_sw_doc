.. _kernel:


Kernel
==================


From Sources
++++++++++++++++++++

1. Get the sources
   .. code-block::
      
      git clone https://github.com/Xilinx/linux-xlnx.git
      cd linux-xlnx
      git checkout xilinx-v2019.2.01

2. Set environment for Cross-Compilation:  
   .. code-block::

      export CROSS_COMPILE=arm-linux-gnueabihf-
      export ARCH=arm

3. Configuration
   .. code-block::
       
      $make xilinx_zynq_defconfig 

4. Build
   .. code-block::

      $ make -j<cores>
      $ mkimage -A arm -O linux -T kernel -C none -a 0x80008000 -e 0x80008000 -n "Linux kernel" -d linux-xlnx/arch/arm/boot/zImage uImage

From script
+++++++++++++++++++++++

Above steps are automated using Makefile and can be found in `spec7 ohwr repo <>`

.. code-block::

   output=../../output

   export CROSS_COMPILE=arm-linux-gnueabihf-
   export ARCH=arm

   defconfig=xilinx_zynq_defconfig 

   kernel := linux-xlnx

   defconfig: 
	@$(MAKE) -C $(kernel) $(defconfig)
	@$(MAKE) -C $(kernel)
	mkimage -A arm -O linux -T kernel -C none -a 0x80008000 -e 0x80008000 -n "Linux kernel" -d linux-xlnx/arch/arm/boot/zImage uImage
	cp uImage $(output)

  menuconfig:
	@$(MAKE) -C $(kernel) menuconfig

  clean: 
	@$(MAKE) -C $(kernel) clean 




This will generate Kernel ``uImage`` in output directory, which can be stored in flash or loaded over TFTP


Petalinux Flow
++++++++++++++++++++++++++

1. Download Petalinux sources from Xilinx official website

2. In Petalinux directory

.. code-block::
   $ source settings.ch

3. To create the Project

.. code-block::
   
   $ petalinux-create --type project --template zynq --name spec7_kernel

4. Copy .xsa generated from Vivado in Project directory and then build 

.. code-block::

   $ petalinux-config --get-hw-description

5. To config u-boot, and Linux

.. code-block::

   $ petalinux-config -c u-boot
   $ petalinux-config -c kernel

6. Now to build everything 

.. code-block:: 

   $ petalinux-build

7. All the generated output will be availabe in ``images`` folder

8. To create final BOOT.BIN, run this inside

.. code-block::
  
   $ petalinux-package --boot --fsbl zynq_fsbl.elf --fpga system.bit --u-boot --kernel


